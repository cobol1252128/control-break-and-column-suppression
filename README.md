** CONBRE1.CBL **
* Make Report REPORT1.RPT
** DATA1.DAT **
Input File for CONBRE1 Program
Sequentail Ordered File
A Col Character 2 Characters
COUNTER Numeric 3 Characters

** CONBRE2.CBL **
* Make Report REPORT2.RPT
** DATA2.DAT **
Input File for CONBRE2 Program
Sequentail Ordered File
A Col Character 2 Characters
B Col Character 2 Characters
COUNTER Numeric 3 Characters

** CONBRE3.CBL **
* Make Report REPORT3.RPT
** DATA3.DAT **
Input File for CONBRE3 Program
Sequentail Ordered File
Branch Col  Character 1 Characters
Date Col    Character 8 Characters
Product Col Character 9 Characters
Income Col  Numeric 3 Characters  
